﻿using System;
using System.Collections.Generic;
using System.Text;

namespace utb_eshop_patek.Domain.Entities.Products
{
    public class Product : Entity
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string ImageURL { get; set; }
    }
}
