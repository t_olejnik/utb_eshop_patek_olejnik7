﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Text;

namespace utb_eshop_patek.Infrastructure.Configuration
{
    public class InfrastructureDependencyModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterAssemblyTypes(ThisAssembly)
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
        }
    }
}
