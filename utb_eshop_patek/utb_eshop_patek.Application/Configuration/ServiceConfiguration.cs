﻿using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using utb_eshop_patek.Infrastructure.Configuration;

namespace utb_eshop_patek.Application.Configuration
{
    public class ServiceConfiguration
    {
        public static void Load(IServiceCollection services, IHostingEnvironment environment)
        {
            services.AddAutoMapper(Assembly.GetAssembly(typeof(ServiceConfiguration)));
            services.AddSingleton(environment);
            InfrastructureServiceConfiguration.Load(services, environment);
        }
    }
}
