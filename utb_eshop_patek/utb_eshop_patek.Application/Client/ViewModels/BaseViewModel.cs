﻿using System;
using System.Collections.Generic;
using System.Text;

namespace utb_eshop_patek.Application.Client.ViewModels
{
    public class BaseViewModel
    {
        public int? UserID { get; set; }
        public string Login { get; set; }
    }
}
