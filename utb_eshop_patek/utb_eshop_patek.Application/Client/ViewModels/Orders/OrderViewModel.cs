﻿using System;
using System.Collections.Generic;
using utb_eshop_patek.Domain.Entities.Orders;

namespace utb_eshop_patek.Application.Client.ViewModels.Orders
{
    public class OrderViewModel
    {
        public IList<OrderItem> OrderItems { get; set; }
        public DateTime DateCreated { get; set; }
        public decimal Total { get; set; }
    }
}